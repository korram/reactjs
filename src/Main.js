import React, { useState } from 'react';
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UploadOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from '@ant-design/icons';

import { Outlet, useLocation } from 'react-router-dom';
import Test1 from './Test1';
import { Layout, Menu, theme } from 'antd';
const { Header, Sider, Content } = Layout;
import {useRouter} from 'next/router';
const Main = () => {

  const router = useRouter();
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const onClick = ( item, key, keyPath, domEvent) =>{
    router.push('/Test1')
  }
  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={collapsed}
      breakpoint="lg"
      // collapsedWidth="0"
      onBreakpoint={(broken) => {
        console.log(broken);
        if (broken) setCollapsed(true);
        else setCollapsed(false)
      }}>
        <div className="logo" />
        <Menu
          theme="dark"
          mode="inline"
          onClick={onClick}
          defaultSelectedKeys={['1']}
          items={[
            {
              key: '1',
              icon: <UserOutlined />,
              label: 'nav 1.1',
            },
            {
              key: '2',
              icon: <VideoCameraOutlined />,
              label: 'nav 2',
              path: 'Test1'
            },
            {
              key: '3',
              icon: <UploadOutlined />,
              label: 'nav 3',
              path: 'Test2'
            },
          ]}
        />
      </Sider>
      <Layout className="site-layout">
        <Header
          style={{
            padding: 0,
            background: colorBgContainer,
          }}
        >
          {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
            className: 'trigger',
            onClick: () => setCollapsed(!collapsed),
          })}
        </Header>
        <Content
          style={{
            margin: '24px 16px',
            padding: 24,
            minHeight: 280,
            background: colorBgContainer,
          }}
        >
          <Outlet/>
        </Content>
      </Layout>
    </Layout>
  );
};
export default Main;

